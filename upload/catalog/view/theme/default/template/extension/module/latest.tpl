			   <div class="container">

<div class="row">
                     <div class="col-lg-12">
                        <div role="tabpanel" class="product-tab-carousel">
                           <!-- Nav tabs -->
                           <div class="header">
                              <h4><?php echo $heading_title; ?></h4>
                             
                           </div>
                           <div class="clearfix"></div>
                           <!-- Tab Panes -->
                           <div class="tab-content">
                              <div role="tabpanel" class="tab-pane active" id="featuredproducts">
                                 <div class="row">
								 <?php foreach ($products as $product) { ?>
                                    <div class="col-lg-3 col-md-3">
                                       <div class="product text-left ">
                                          <span class="label label-hot rotated left">NEW</span>
                                          <div class="product-top">
                                             
                                             
                                                 <div class="image"><a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive" /></a></div>
                                             
                                          </div>
                                          <!-- End .product-top -->
                                          <div class="product_hover">
                                             
                                             <!-- End .product-cats -->
                                             <h2 class="product-title"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h2>
                                             <div class="product-price-container"> 						    <?php if ($product['price']) { ?>
            
                  <?php if (!$product['special']) { ?>
				  <span class="product-price"> <?php echo $product['price']; ?></span>
                 
                  <?php } else { ?>
				 <span class="product-old-price"><?php echo $product['price']; ?></span><span class="product-price"><?php echo $product['special']; ?></span>
                 
                  <?php } ?>
              
               
                <?php } ?></div>
                                             <!-- End .product-price-container -->
                                             
                                             <!-- End .product-ratings -->
                                             <div class="product-action-container"> </div>
                                             <!-- end .product-action-container -->
                                             <a href="index.php?route=account/registerorder&product_name=<?php echo $heading_title; ?>" class="list-btn list-btn-add" title="Add to cart">Order Now</a>
                                          </div>
                                          <!-- End .product -->
                                       </div>
                                    </div>
                                   
                                     <?php } ?>
                                    
                                 </div>
								
                                 <!-- end .product-featured-carousel-sm -->
                              </div>
                              <!-- End .tab-pane -->
                             
                              <!-- End .tab-pane -->
                              
                              <!-- End .tab-pane -->
                           </div>
                           <!-- End .tab-content -->
                        </div>
                        <!-- end role[tabpanel] -->
                     </div>
                  </div>
				  </div>
