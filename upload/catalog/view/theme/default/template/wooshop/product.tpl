<?php echo $header; ?>
            <!-- End #header -->
            <section id="content" role="main">
               <div class="container">
                  <ol class="breadcrumb">
                     <li><a href="index.html">Home</a></li>
                     <li><a href="category.html">Category</a></li>
                     <li class="active">Product</li>
                  </ol>
               </div>
               <!-- End .container -->
               <div class="container">
                  <div class="row">
                     <div class="col-sm-6">
                        <div class="product-gallery-container">
                           <div class="product-top">
						      <?php if ($thumb) { ?>
          
          
                              <img id="product-zoom" src="<?php echo $thumb; ?>" data-zoom-image="<?php echo $thumb; ?>" href="<?php echo $popup; ?>" title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>"/>
							    <?php } ?>
                           </div>
                           <!-- End .product-top -->
                           <div class="product-gallery-wrapper">
                              <div class="owl-carousel product-gallery">
							  
							    <?php if ($images) { ?>
            <?php foreach ($images as $image) { ?>
			 <a href="<?php echo $image['popup']; ?>" title="<?php echo $heading_title; ?>" data-image="<?php echo $image['thumb']; ?>" data-zoom-image="<?php echo $image['thumb']; ?>" class="product-gallery-item">
                                 <img src="<?php echo $image['thumb']; ?>" alt="product-small-1">
                                 </a>
		
            <?php } ?>
            <?php } ?>
                                
                                
                              </div>
                              <!-- End .product-gallery -->
                           </div>
                           <!-- End #product-gallery-wrapper -->
                        </div>
                        <!-- End .product-gallery-container -->
                     </div>
                     <!-- End .col-sm-6 -->
                     <div class="col-sm-6">
                        <div class="product-details">
                           <h2 class="product-title"> <h1><?php echo $heading_title; ?></h1></h2>
						     <?php if ($manufacturer) { ?>
           
           
                           <div class="product-cats">
						   
						   <a href="#" title="Category Name">  <li><?php echo $text_manufacturer; ?> <a href="<?php echo $manufacturers; ?>"><?php echo $manufacturer; ?></a></li></a>
	
						    <?php } ?>
							 <li><?php echo $text_model; ?> <?php echo $model; ?></li>
            <?php if ($reward) { ?>
            <li><?php echo $text_reward; ?> <?php echo $reward; ?></li>
            <?php } ?>
            <li><?php echo $text_stock; ?> <?php echo $stock; ?></li>
						   </div>
                           <!-- End .product-cats -->
                           <div class="product-ratings-container">
                              <div class="product-ratings">
                                 <span class="star active"></span>
                                 <span class="star active"></span>
                                 <span class="star active"></span>
                                 <span class="star"></span>
                                 <span class="star"></span>
                              </div>
                              <!-- End .product-ratings -->
                              <span class="product-ratings-count">25 Reviews</span>
                           </div>
                           <!-- End .product-ratings-container -->
                           <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipiscing elit, eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                           <div class="product-price-container">
						    <?php if ($price) { ?>
            
                  <?php if (!$special) { ?>
				  <span class="product-price"> <?php echo $price; ?></span>
                 
                  <?php } else { ?>
				 <span class="product-old-price"><?php echo $price; ?></span><span class="product-price"><?php echo $special; ?></span>
                 
                  <?php } ?>
              
               
                <?php } ?>
						   
                             
                           </div>
                           <!-- End .product-price-container -->
                       
                           <!-- End .filter-size-container -->
                         
                           <!-- End .filter-color-container -->
                         
                           <!-- End .product-quantity-wrapper -->
                           <div class="product-action">
                              <a href="index.php?route=account/registerorder&product_name=<?php echo $heading_title; ?>" class="list-btn list-btn-add" title="Add to cart">Order Now</a>
                             
                           </div>
                           <!-- End .product-action -->
                           <div class="product-tags"><span>Tags:</span><a href="#" title="Tag Name">bootstrap</a><a href="#" title="Tag Name">collections</a><a href="#" title="Tag Name">color</a><a href="#" title="Tag Name">responsive</a></div>
                           <!-- End .product-tags -->
                          
                           <!-- End .share-box -->
                        </div>
                        <!-- End .product-details -->
                     </div>
                     <!-- End .col-sm-6 -->
                  </div>
                  <!-- End .row -->
               </div>
               <!-- End .container -->
               <div class="container">
                  <div class="row">
                     <div class="col-md-12">
                        <div role="tabpanel" class="product-details-tab">
                           <!-- Nav tabs -->
                           <ul class="nav nav-tabs" role="tablist">
                              <li role="presentation"><a href="#description" aria-controls="description" role="tab" data-toggle="tab" title="Product Description">Product Description</a></li>
                              <li role="presentation"  class="active"><a href="#shipping" aria-controls="shipping" role="tab" data-toggle="tab" title="Shipping &amp; Returns">Specification</a></li>
                              <li role="presentation"><a href="#reviews" aria-controls="reviews" role="tab" data-toggle="tab" title="Reviews">Reviews (25)</a></li>
                           </ul>
                           <!-- Tab Panes -->
                           <div class="tab-content">
                              <div role="tabpanel" class="tab-pane" id="description">
                               <?php echo $description; ?>
                              </div>
                              <!-- End .tab-pane -->
                              <div role="tabpanel" class="tab-pane active" id="shipping">
                                 <div class="row">
                                    <div class="col-sm-6">
                                        <table class="table table-bordered">
                <?php foreach ($attribute_groups as $attribute_group) { ?>
                <thead>
                  <tr>
                    <td colspan="2"><strong><?php echo $attribute_group['name']; ?></strong></td>
                  </tr>
                </thead>
                <tbody>
                  <?php foreach ($attribute_group['attribute'] as $attribute) { ?>
                  <tr>
                    <td><?php echo $attribute['name']; ?></td>
                    <td><?php echo $attribute['text']; ?></td>
                  </tr>
                  <?php } ?>
                </tbody>
                <?php } ?>
              </table>
                                    </div>
                                    <!-- End .col-sm-6 -->
                                    <div class="clearfix visible-xs md-margin"></div>
                                    <!-- space -->
                                
                                    <!-- End .col-sm-6 -->
                                 </div>
                                 <!-- End .row -->
                              </div>
                              <!-- End .tab-pane -->
                              <div role="tabpanel" class="tab-pane" id="reviews">
                                 <div id="respond" class="review-respond">
                                    <h3>Leave a Review</h3>
                                    <form action="#" method="get">
                                       <div class="row">
                                          <div class="col-sm-6">
                                             <div class="form-group">
                                                <label for="name" class="input-desc">Your Name <span class="required-field">*</span></label>
                                                <input type="text" class="form-control" id="name" name="name" placeholder="Your name (required)" required>
                                             </div>
                                             <!-- End .from-group -->
                                             <div class="form-group">
                                                <label for="email" class="input-desc">Your Email <span class="required-field">*</span></label>
                                                <input type="email" class="form-control" id="email" name="email" placeholder="Your email address (required)" required>
                                             </div>
                                             <!-- End .from-group -->
                                          </div>
                                          <!-- End .col-sm-6 -->
                                       </div>
                                       <!-- End .row -->
                                       <div class="form-group">
                                          <label for="email" class="input-desc xs-margin">Your Rating<span class="required-field">*</span></label>
                                          <div class="product-ratings">
                                             <span class="star active"></span>
                                             <span class="star active"></span>
                                             <span class="star active"></span>
                                             <span class="star"></span>
                                             <span class="star"></span>
                                          </div>
                                          <!-- End .product-ratings -->
                                       </div>
                                       <!-- End .from-group -->
                                       <div class="form-group last">
                                          <label for="message" class="input-desc">Your Message <span class="required-field">*</span></label>
                                          <textarea class="form-control" rows="7" id="message" name="message" placeholder="Your message content (required)" required></textarea>
                                       </div>
                                       <!-- End .from-group -->
                                       <input type="submit" class="btn btn-custom2 min-width" value="Send Review">
                                    </form>
                                 </div>
                                 <!-- End #respond -->
                              </div>
                              <!-- End .tab-pane -->
                           </div>
                           <!-- End .tab-content -->
                        </div>
                        <!-- end role[tabpanel] -->
                     </div>
                     <!-- End .col-md-12 -->
                  </div>
                  <!-- End .row -->
               </div>
               <!-- End .container -->
               <div class="container">
                  <div class="row">
                     <div class="col-lg-12">
                        <div role="tabpanel" class="product-tab-carousel">
                           <!-- Nav tabs -->
                           <div class="header">
                              <h4>Related Product</h4>
                              <ul class="nav nav-lava nav-lava-sm" role="tablist">
                                 <li role="presentation" class="active"><a href="#featuredproducts" aria-controls="featuredproducts" role="tab" data-toggle="tab">Bras</a></li>
                                 <li role="presentation"><a href="#popularproducts" aria-controls="popularproducts" role="tab" data-toggle="tab">Panties</a></li>
                                 <li role="presentation"><a href="#newarrivals" aria-controls="newarrivals" role="tab" data-toggle="tab">Swim</a></li>
                              </ul>
                           </div>
                           <div class="clearfix"></div>
                           <!-- Tab Panes -->
                           <div class="tab-content">
                              <div role="tabpanel" class="tab-pane active" id="featuredproducts">
                                 <div class="row">
                                    <div class="col-lg-3 col-md-3">
                                       <div class="product text-left ">
                                          <span class="label label-hot rotated left">Hot</span>
                                          <div class="product-top">
                                             <a href="#" class="product-btn btn-icon" title="Compare"><i class="fa fa-retweet"></i></a>
                                             <a href="#" class="product-btn btn-icon top-right" title="Favorite"><i class="fa fa-heart"></i></a>
                                             <figure class="owl-carousel product-slider">
                                                <a href="product.html" title="Pullover Batwing Sleeve Zigzag">
                                                <img src="images/products/index2/33.jpg" alt="Product image" class="product-image">
                                                </a>
                                                <a href="product.html" title="Pullover Batwing Sleeve Zigzag">
                                                <img src="images/products/index2/34.jpg" alt="Product image" class="product-image">
                                                </a>
                                             </figure>
                                          </div>
                                          <!-- End .product-top -->
                                          <div class="product_hover">
                                             <div class="product-cats"><a href="#" title="Category Name">Woman clothing</a></div>
                                             <!-- End .product-cats -->
                                             <h2 class="product-title"><a href="product.html" title="Pullover Batwing Sleeve Zigzag">Blue sleeve</a></h2>
                                             <div class="product-price-container"> <span class="product-old-price">$89.99</span> <span class="product-price">$69.99</span> </div>
                                             <!-- End .product-price-container -->
                                             <div class="product-ratings"> <span class="star active"></span> <span class="star active"></span> <span class="star active"></span> <span class="star active"></span> <span class="star"></span> </div>
                                             <!-- End .product-ratings -->
                                             <div class="product-action-container"> </div>
                                             <!-- end .product-action-container -->
                                             <a href="#" class="product-btn product-add-btn"><i class="fa fa-shopping-cart"></i>Add to cart</a>
                                          </div>
                                          <!-- End .product -->
                                       </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3">
                                       <div class="product text-left ">
                                          <div class="product-top">
                                             <a href="#" class="product-btn btn-icon" title="Compare"><i class="fa fa-retweet"></i></a>
                                             <a href="#" class="product-btn btn-icon top-right" title="Favorite"><i class="fa fa-heart"></i></a>
                                             <figure class="owl-carousel product-slider">
                                                <a href="product.html" title="Pullover Batwing Sleeve Zigzag">
                                                <img src="images/products/index2/30.jpg" alt="Product image" class="product-image">
                                                </a>
                                                <a href="product.html" title="Pullover Batwing Sleeve Zigzag">
                                                <img src="images/products/index2/31.jpg" alt="Product image" class="product-image">
                                                </a>
                                             </figure>
                                          </div>
                                          <!-- End .product-top -->
                                          <div class="product_hover">
                                             <div class="product-cats"><a href="#" title="Category Name">Woman clothing</a></div>
                                             <!-- End .product-cats -->
                                             <h2 class="product-title"><a href="product.html" title="Pullover Batwing Sleeve Zigzag">Sexy Magenta Gift Wrap Bra</a></h2>
                                             <div class="product-price-container"> <span class="product-old-price">$84.99</span> <span class="product-price">$75.95</span> </div>
                                             <!-- End .product-price-container -->
                                             <div class="product-ratings"> <span class="star active"></span> <span class="star active"></span> <span class="star active"></span> <span class="star"></span> <span class="star"></span> </div>
                                             <!-- End .product-ratings -->
                                             <div class="product-action-container"> </div>
                                             <!-- end .product-action-container -->
                                             <a href="#" class="product-btn product-add-btn"><i class="fa fa-shopping-cart"></i>Add to cart</a>
                                          </div>
                                          <!-- End .product -->
                                       </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3">
                                       <div class="product text-left">
                                          <span class="label label-sale rotated left">Sale</span>
                                          <div class="product-top">
                                             <a href="#" class="product-btn btn-icon" title="Compare"><i class="fa fa-retweet"></i></a>
                                             <a href="#" class="product-btn btn-icon top-right" title="Favorite"><i class="fa fa-heart"></i></a>
                                             <figure class="owl-carousel product-slider">
                                                <a href="product.html" title="Pullover Batwing Sleeve Zigzag">
                                                <img src="images/products/index2/21.jpg" alt="Product image" class="product-image">
                                                </a>
                                                <a href="product.html" title="Pullover Batwing Sleeve Zigzag">
                                                <img src="images/products/index2/20.jpg" alt="Product image" class="product-image">
                                                </a>
                                             </figure>
                                          </div>
                                          <!-- End .product-top -->
                                          <div class="product_hover">
                                             <div class="product-cats"><a href="#" title="Category Name">Woman clothing</a></div>
                                             <!-- End .product-cats -->
                                             <h2 class="product-title"><a href="product.html" title="Pullover Batwing Sleeve Zigzag">White Maya Twist Bandeau bra</a></h2>
                                             <div class="product-price-container"> <span class="product-old-price">$65.99</span> <span class="product-price">$45.99</span> </div>
                                             <!-- End .product-price-container -->
                                             <div class="product-ratings"> <span class="star active"></span> <span class="star active"></span> <span class="star"></span> <span class="star"></span> <span class="star"></span> </div>
                                             <!-- End .product-ratings -->
                                             <div class="product-action-container"> </div>
                                             <!-- end .product-action-container -->
                                             <a href="#" class="product-btn product-add-btn"><i class="fa fa-shopping-cart"></i>Add to cart</a>
                                          </div>
                                          <!-- End .product -->
                                       </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3">
                                       <div class="product text-left ">
                                          <span class="label label-hot rotated left">Hot</span>
                                          <div class="product-top">
                                             <a href="#" class="product-btn btn-icon" title="Compare"><i class="fa fa-retweet"></i></a>
                                             <a href="#" class="product-btn btn-icon top-right" title="Favorite"><i class="fa fa-heart"></i></a>
                                             <figure class="owl-carousel product-slider">
                                                <a href="product.html" title="Pullover Batwing Sleeve Zigzag">
                                                <img src="images/products/index2/29.jpg" alt="Product image" class="product-image">
                                                </a>
                                                <a href="product.html" title="Pullover Batwing Sleeve Zigzag">
                                                <img src="images/products/index2/28.jpg" alt="Product image" class="product-image">
                                                </a>
                                             </figure>
                                          </div>
                                          <!-- End .product-top -->
                                          <div class="product_hover">
                                             <div class="product-cats"><a href="#" title="Category Name">Woman clothing</a></div>
                                             <!-- End .product-cats -->
                                             <h2 class="product-title"><a href="product.html" title="Pullover Batwing Sleeve Zigzag">Pink Stretch Mesh Nightie</a></h2>
                                             <div class="product-price-container"> <span class="product-old-price">$169.99</span> <span class="product-price">$119.99</span> </div>
                                             <!-- End .product-price-container -->
                                             <div class="product-ratings"> <span class="star active"></span> <span class="star active"></span> <span class="star active"></span> <span class="star active"></span> <span class="star active"></span> </div>
                                             <!-- End .product-ratings -->
                                             <div class="product-action-container"> </div>
                                             <!-- end .product-action-container -->
                                             <a href="#" class="product-btn product-add-btn"><i class="fa fa-shopping-cart"></i>Add to cart</a>
                                          </div>
                                          <!-- End .product -->
                                       </div>
                                    </div>
                                 </div>
                                 <!-- end .product-featured-carousel-sm -->
                              </div>
                              <!-- End .tab-pane -->
                              <div role="tabpanel" class="tab-pane" id="popularproducts">
                                 <div class="row">
                                    <div class="col-lg-3">
                                       <div class="product text-left">
                                          <div class="product-top">
                                             <a href="#" class="product-btn btn-icon" title="Compare"><i class="fa fa-retweet"></i></a>
                                             <a href="#" class="product-btn btn-icon top-right" title="Favorite"><i class="fa fa-heart"></i></a>
                                             <figure class="owl-carousel product-slider">
                                                <a href="product.html" title="Pullover Batwing Sleeve Zigzag">
                                                <img src="images/products/index2/27.jpg" alt="Product image" class="product-image">
                                                </a>
                                                <a href="product.html" title="Pullover Batwing Sleeve Zigzag">
                                                <img src="images/products/index2/26.jpg" alt="Product image" class="product-image">
                                                </a>
                                             </figure>
                                          </div>
                                          <!-- End .product-top -->
                                          <div class="product_hover">
                                             <div class="product-cats"><a href="#" title="Category Name">Woman clothing</a></div>
                                             <!-- End .product-cats -->
                                             <h2 class="product-title"><a href="product.html" title="Pullover Batwing Sleeve Zigzag">Pantie and G-string Sexy Lingeries </a></h2>
                                             <div class="product-price-container"> <span class="product-old-price">$85.99</span> <span class="product-price">$65.99</span> </div>
                                             <!-- End .product-price-container -->
                                             <div class="product-ratings"> <span class="star active"></span> <span class="star active"></span> <span class="star active"></span> <span class="star active"></span> <span class="star active"></span> </div>
                                             <!-- End .product-ratings -->
                                             <div class="product-action-container"> </div>
                                             <!-- end .product-action-container -->
                                             <a href="#" class="product-btn product-add-btn"><i class="fa fa-shopping-cart"></i>Add to cart</a>
                                          </div>
                                          <!-- End .product -->
                                       </div>
                                    </div>
                                    <div class="col-lg-3">
                                       <div class="product text-left">
                                          <span class="label label-sale rotated left">Sale</span>
                                          <div class="product-top">
                                             <a href="#" class="product-btn btn-icon" title="Compare"><i class="fa fa-retweet"></i></a>
                                             <a href="#" class="product-btn btn-icon top-right" title="Favorite"><i class="fa fa-heart"></i></a>
                                             <figure class="owl-carousel product-slider">
                                                <a href="product.html" title="Pullover Batwing Sleeve Zigzag">
                                                <img src="images/products/index2/29.jpg" alt="Product image" class="product-image">
                                                </a>
                                                <a href="product.html" title="Pullover Batwing Sleeve Zigzag">
                                                <img src="images/products/index2/28.jpg" alt="Product image" class="product-image">
                                                </a>
                                             </figure>
                                          </div>
                                          <!-- End .product-top -->
                                          <div class="product_hover">
                                             <div class="product-cats"><a href="#" title="Category Name">Woman clothing</a></div>
                                             <!-- End .product-cats -->
                                             <h2 class="product-title"><a href="product.html" title="Pullover Batwing Sleeve Zigzag">Floral Lace Panty Baby Lingeries</a></h2>
                                             <div class="product-price-container"> <span class="product-old-price">$89.99</span> <span class="product-price">$59.99</span> </div>
                                             <!-- End .product-price-container -->
                                             <div class="product-ratings"> <span class="star active"></span> <span class="star active"></span> <span class="star active"></span> <span class="star active"></span> <span class="star active"></span> </div>
                                             <!-- End .product-ratings -->
                                             <div class="product-action-container"> </div>
                                             <!-- end .product-action-container -->
                                             <a href="#" class="product-btn product-add-btn"><i class="fa fa-shopping-cart"></i>Add to cart</a>
                                          </div>
                                          <!-- End .product -->
                                       </div>
                                    </div>
                                    <div class="col-lg-3">
                                       <div class="product text-left">
                                          <span class="label label-sale rotated left">Sale</span>
                                          <div class="product-top">
                                             <a href="#" class="product-btn btn-icon" title="Compare"><i class="fa fa-retweet"></i></a>
                                             <a href="#" class="product-btn btn-icon top-right" title="Favorite"><i class="fa fa-heart"></i></a>
                                             <figure class="owl-carousel product-slider">
                                                <a href="product.html" title="Pullover Batwing Sleeve Zigzag">
                                                <img src="images/products/index2/37.jpg" alt="Product image" class="product-image">
                                                </a>
                                                <a href="product.html" title="Pullover Batwing Sleeve Zigzag">
                                                <img src="images/products/index2/38.jpg" alt="Product image" class="product-image">
                                                </a>
                                             </figure>
                                          </div>
                                          <!-- End .product-top -->
                                          <div class="product_hover">
                                             <div class="product-cats"><a href="#" title="Category Name">Woman clothing</a></div>
                                             <!-- End .product-cats -->
                                             <h2 class="product-title"><a href="product.html" title="Pullover Batwing Sleeve Zigzag">Baby Cake Bra Baby Dolls</a></h2>
                                             <div class="product-price-container"> <span class="product-old-price">$75.99</span> <span class="product-price">$70.99</span> </div>
                                             <!-- End .product-price-container -->
                                             <div class="product-ratings"> <span class="star active"></span> <span class="star active"></span> <span class="star active"></span> <span class="star active"></span> <span class="star active"></span> </div>
                                             <!-- End .product-ratings -->
                                             <div class="product-action-container"> </div>
                                             <!-- end .product-action-container -->
                                             <a href="#" class="product-btn product-add-btn"><i class="fa fa-shopping-cart"></i>Add to cart</a>
                                          </div>
                                          <!-- End .product -->
                                       </div>
                                    </div>
                                    <div class="col-lg-3">
                                       <div class="product text-left">
                                          <div class="product-top">
                                             <a href="#" class="product-btn btn-icon" title="Compare"><i class="fa fa-retweet"></i></a>
                                             <a href="#" class="product-btn btn-icon top-right" title="Favorite"><i class="fa fa-heart"></i></a>
                                             <figure class="owl-carousel product-slider">
                                                <a href="product.html" title="Pullover Batwing Sleeve Zigzag">
                                                <img src="images/products/index2/40.jpg" alt="Product image" class="product-image">
                                                </a>
                                                <a href="product.html" title="Pullover Batwing Sleeve Zigzag">
                                                <img src="images/products/index2/39.jpg" alt="Product image" class="product-image">
                                                </a>
                                             </figure>
                                          </div>
                                          <!-- End .product-top -->
                                          <div class="product_hover">
                                             <div class="product-cats"><a href="#" title="Category Name">Woman clothing</a></div>
                                             <!-- End .product-cats -->
                                             <h2 class="product-title"><a href="product.html" title="Pullover Batwing Sleeve Zigzag">About This Frisky Kitty Leopard </a></h2>
                                             <div class="product-price-container"> <span class="product-old-price">$84.99</span> <span class="product-price">$64.99</span> </div>
                                             <!-- End .product-price-container -->
                                             <div class="product-ratings"> <span class="star active"></span> <span class="star active"></span> <span class="star active"></span> <span class="star active"></span> <span class="star active"></span> </div>
                                             <!-- End .product-ratings -->
                                             <div class="product-action-container"> </div>
                                             <!-- end .product-action-container -->
                                             <a href="#" class="product-btn product-add-btn"><i class="fa fa-shopping-cart"></i>Add to cart</a>
                                          </div>
                                          <!-- End .product -->
                                       </div>
                                    </div>
                                 </div>
                                 <!-- end .product-popular-carousel -->
                              </div>
                              <!-- End .tab-pane -->
                              <div role="tabpanel" class="tab-pane" id="newarrivals">
                                 <div class="row">
                                    <div class="col-lg-3">
                                       <div class="product text-left">
                                          <span class="label label-hot rotated left">Hot</span>
                                          <div class="product-top">
                                             <a href="#" class="product-btn btn-icon" title="Compare"><i class="fa fa-retweet"></i></a>
                                             <a href="#" class="product-btn btn-icon top-right" title="Favorite"><i class="fa fa-heart"></i></a>
                                             <figure class="owl-carousel product-slider">
                                                <a href="product.html" title="Pullover Batwing Sleeve Zigzag">
                                                <img src="images/products/index2/1.jpg" alt="Product image" class="product-image">
                                                </a>
                                                <a href="product.html" title="Pullover Batwing Sleeve Zigzag">
                                                <img src="images/products/index2/41.jpg" alt="Product image" class="product-image">
                                                </a>
                                             </figure>
                                          </div>
                                          <!-- End .product-top -->
                                          <div class="product_hover">
                                             <div class="product-cats"><a href="#" title="Category Name">Woman clothing</a></div>
                                             <!-- End .product-cats -->
                                             <h2 class="product-title"><a href="product.html" title="Pullover Batwing Sleeve Zigzag">Sexy Strapless Bandeau Top</a></h2>
                                             <div class="product-price-container"> <span class="product-old-price">$85.99</span> <span class="product-price">$75.99</span> </div>
                                             <!-- End .product-price-container -->
                                             <div class="product-ratings"> <span class="star active"></span> <span class="star active"></span> <span class="star active"></span> <span class="star active"></span> <span class="star active"></span> </div>
                                             <!-- End .product-ratings -->
                                             <div class="product-action-container"> </div>
                                             <!-- end .product-action-container -->
                                             <a href="#" class="product-btn product-add-btn"><i class="fa fa-shopping-cart"></i>Add to cart</a>
                                          </div>
                                          <!-- End .product -->
                                       </div>
                                    </div>
                                    <div class="col-lg-3">
                                       <div class="product text-left">
                                          <span class="label label-hot rotated left">Hot</span>
                                          <div class="product-top">
                                             <a href="#" class="product-btn btn-icon" title="Compare"><i class="fa fa-retweet"></i></a>
                                             <a href="#" class="product-btn btn-icon top-right" title="Favorite"><i class="fa fa-heart"></i></a>
                                             <figure class="owl-carousel product-slider">
                                                <a href="product.html" title="Pullover Batwing Sleeve Zigzag">
                                                <img src="images/products/index2/13.jpg" alt="Product image" class="product-image">
                                                </a>
                                                <a href="product.html" title="Pullover Batwing Sleeve Zigzag">
                                                <img src="images/products/index2/12.jpg" alt="Product image" class="product-image">
                                                </a>
                                             </figure>
                                          </div>
                                          <!-- End .product-top -->
                                          <div class="product_hover">
                                             <div class="product-cats"><a href="#" title="Category Name">Woman clothing</a></div>
                                             <!-- End .product-cats -->
                                             <h2 class="product-title"><a href="product.html" title="Pullover Batwing Sleeve Zigzag">White Embroidered Shelf Open Bra</a></h2>
                                             <div class="product-price-container"> <span class="product-old-price">$95.99</span> <span class="product-price">$65.99</span> </div>
                                             <!-- End .product-price-container -->
                                             <div class="product-ratings"> <span class="star active"></span> <span class="star active"></span> <span class="star active"></span> <span class="star active"></span> <span class="star active"></span> </div>
                                             <!-- End .product-ratings -->
                                             <div class="product-action-container"> </div>
                                             <!-- end .product-action-container -->
                                             <a href="#" class="product-btn product-add-btn"><i class="fa fa-shopping-cart"></i>Add to cart</a>
                                          </div>
                                          <!-- End .product -->
                                       </div>
                                    </div>
                                    <div class="col-lg-3">
                                       <div class="product text-left">
                                          <div class="product-top">
                                             <a href="#" class="product-btn btn-icon" title="Compare"><i class="fa fa-retweet"></i></a>
                                             <a href="#" class="product-btn btn-icon top-right" title="Favorite"><i class="fa fa-heart"></i></a>
                                             <figure class="owl-carousel product-slider">
                                                <a href="product.html" title="Pullover Batwing Sleeve Zigzag">
                                                <img src="images/products/index2/9.jpg" alt="Product image" class="product-image">
                                                </a>
                                                <a href="product.html" title="Pullover Batwing Sleeve Zigzag">
                                                <img src="images/products/index2/2.jpg" alt="Product image" class="product-image">
                                                </a>
                                             </figure>
                                          </div>
                                          <!-- End .product-top -->
                                          <div class="product_hover">
                                             <div class="product-cats"><a href="#" title="Category Name">Woman clothing</a></div>
                                             <!-- End .product-cats -->
                                             <h2 class="product-title"><a href="product.html" title="Pullover Batwing Sleeve Zigzag">Sexy Magenta Gift Wrap Bra and Panty</a></h2>
                                             <div class="product-price-container"> <span class="product-old-price">$82.99</span> <span class="product-price">$76.99</span> </div>
                                             <!-- End .product-price-container -->
                                             <div class="product-ratings"> <span class="star active"></span> <span class="star active"></span> <span class="star active"></span> <span class="star active"></span> <span class="star active"></span> </div>
                                             <!-- End .product-ratings -->
                                             <div class="product-action-container"> </div>
                                             <!-- end .product-action-container -->
                                             <a href="#" class="product-btn product-add-btn"><i class="fa fa-shopping-cart"></i>Add to cart</a>
                                          </div>
                                          <!-- End .product -->
                                       </div>
                                    </div>
                                    <div class="col-lg-3">
                                       <div class="product text-left">
                                          <div class="product-top">
                                             <a href="#" class="product-btn btn-icon" title="Compare"><i class="fa fa-retweet"></i></a>
                                             <a href="#" class="product-btn btn-icon top-right" title="Favorite"><i class="fa fa-heart"></i></a>
                                             <figure class="owl-carousel product-slider">
                                                <a href="product.html" title="Pullover Batwing Sleeve Zigzag">
                                                <img src="images/products/index2/17.jpg" alt="Product image" class="product-image">
                                                </a>
                                                <a href="product.html" title="Pullover Batwing Sleeve Zigzag">
                                                <img src="images/products/index2/16.jpg" alt="Product image" class="product-image">
                                                </a>
                                             </figure>
                                          </div>
                                          <!-- End .product-top -->
                                          <div class="product_hover">
                                             <div class="product-cats"><a href="#" title="Category Name">Woman clothing</a></div>
                                             <!-- End .product-cats -->
                                             <h2 class="product-title"><a href="product.html" title="Pullover Batwing Sleeve Zigzag">Dark Blue Pullover Batwing Sleeve Top</a></h2>
                                             <div class="product-price-container"> <span class="product-old-price">$74.99</span> <span class="product-price">$60.99</span> </div>
                                             <!-- End .product-price-container -->
                                             <div class="product-ratings"> <span class="star active"></span> <span class="star active"></span> <span class="star active"></span> <span class="star active"></span> <span class="star active"></span> </div>
                                             <!-- End .product-ratings -->
                                             <div class="product-action-container"> </div>
                                             <!-- end .product-action-container -->
                                             <a href="#" class="product-btn product-add-btn"><i class="fa fa-shopping-cart"></i>Add to cart</a>
                                          </div>
                                          <!-- End .product -->
                                       </div>
                                    </div>
                                 </div>
                                 <!-- end .product-newarrivals-carousel-sm -->
                              </div>
                              <!-- End .tab-pane -->
                           </div>
                           <!-- End .tab-content -->
                        </div>
                        <!-- end role[tabpanel] -->
                     </div>
                  </div>
               </div>
               <!-- End .container -->
            </section>
            <!-- End #content -->
            <footer id="footer" class="footer-simple footer-dark">
               <div id="footer-top">
                  <div class="container">
                     <div class="row">
                        <div class="col-sm-4">
                           <div class="widget">
                              <h4 class="widget-title">Featured products</h4>
                              <ul class="products-list">
                                 <li>
                                    <figure> <a href="product.html" title="Product Name"> <img src="images/products/thumbnails/product1.jpg" alt="Product image" class="product-image"> </a> </figure>
                                    <h5 class="product-title"><a href="#" title="Ladies Pullover Batwing Sleeve Zigzag">Pantie and G-string Sexy Lingeries</a></h5>
                                    <div class="product-price-container"> <span class="product-old-price">$52.99</span> <span class="product-price">$42.99</span> </div>
                                    <!-- End .product-price-container -->
                                 </li>
                                 <li>
                                    <figure> <a href="product.html" title="Product Name"> <img src="images/products/thumbnails/product2.jpg" alt="Product image" class="product-image"> </a> </figure>
                                    <h5 class="product-title"><a href="#" title="Ladies Pullover Batwing Sleeve Zigzag">Floral Lace Panty Baby Lingeries</a></h5>
                                    <div class="product-price-container"> <span class="product-old-price">$95.99</span> <span class="product-price">$65.99</span> </div>
                                    <!-- End .product-price-container -->
                                 </li>
                                 <li>
                                    <figure> <a href="product.html" title="Product Name"> <img src="images/products/thumbnails/product3.jpg" alt="Product image" class="product-image"> </a> </figure>
                                    <h5 class="product-title"><a href="#" title="Ladies Pullover Batwing Sleeve Zigzag">Baby Cake Bra Baby Dolls Leopard</a></h5>
                                    <div class="product-price-container"> <span class="product-old-price">$70.99</span> <span class="product-price">$64.99</span> </div>
                                    <!-- End .product-price-container -->
                                 </li>
                              </ul>
                              <!-- End .products-list -->
                           </div>
                           <!-- End .widget -->
                        </div>
                        <!-- End .col-md-4 -->
                        <div class="col-sm-4">
                           <div class="widget">
                              <h4 class="widget-title">Top products</h4>
                              <ul class="products-list">
                                 <li>
                                    <figure> <a href="product.html" title="Product Name"> <img src="images/products/thumbnails/product4.jpg" alt="Product image" class="product-image"> </a> </figure>
                                    <h5 class="product-title"><a href="#" title="Ladies Pullover Batwing Sleeve Zigzag">About This Frisky Kitty Lingeries</a></h5>
                                    <div class="product-price-container"> <span class="product-old-price">$96.99</span> <span class="product-price">$58.99</span> </div>
                                    <!-- End .product-price-container -->
                                 </li>
                                 <li>
                                    <figure> <a href="product.html" title="Product Name"> <img src="images/products/thumbnails/product5.jpg" alt="Product image" class="product-image"> </a> </figure>
                                    <h5 class="product-title"><a href="#" title="Ladies Pullover Batwing Sleeve Zigzag">Sexy Strapless Bandeau Top Lingeries</a></h5>
                                    <div class="product-price-container"> <span class="product-old-price">$85.99</span> <span class="product-price">$66.99</span> </div>
                                    <!-- End .product-price-container -->
                                 </li>
                                 <li>
                                    <figure> <a href="product.html" title="Product Name"> <img src="images/products/thumbnails/product6.jpg" alt="Product image" class="product-image"> </a> </figure>
                                    <h5 class="product-title"><a href="#" title="Ladies Pullover Batwing Sleeve Zigzag">White Embroidered Shelf Open Lingeries</a></h5>
                                    <div class="product-price-container"> <span class="product-old-price">$76.99</span> <span class="product-price">$67.99</span> </div>
                                    <!-- End .product-price-container -->
                                 </li>
                              </ul>
                              <!-- End .products-list -->
                           </div>
                           <!-- End .widget -->
                        </div>
                        <!-- End .col-md-4 -->
                        <div class="col-sm-4">
                           <div class="widget">
                              <h4 class="widget-title">Popular products</h4>
                              <ul class="products-list">
                                 <li>
                                    <figure> <a href="product.html" title="Product Name"> <img src="images/products/thumbnails/product7.jpg" alt="Product image" class="product-image"> </a> </figure>
                                    <h5 class="product-title"><a href="#" title="Ladies Pullover Batwing Sleeve Zigzag">White Embroidered Shelf Open Bra</a></h5>
                                    <div class="product-price-container"> <span class="product-old-price">$95.99</span> <span class="product-price">$75.99</span> </div>
                                    <!-- End .product-price-container -->
                                 </li>
                                 <li>
                                    <figure> <a href="product.html" title="Product Name"> <img src="images/products/thumbnails/product8.jpg" alt="Product image" class="product-image"> </a> </figure>
                                    <h5 class="product-title"><a href="#" title="Ladies Pullover Batwing Sleeve Zigzag">Sexy Magenta Gift Wrap Bra and Panty</a></h5>
                                    <div class="product-price-container"> <span class="product-old-price">$85.99</span> <span class="product-price">$58.99</span> </div>
                                    <!-- End .product-price-container -->
                                 </li>
                                 <li>
                                    <figure> <a href="product.html" title="Product Name"> <img src="images/products/thumbnails/product9.jpg" alt="Product image" class="product-image"> </a> </figure>
                                    <h5 class="product-title"><a href="#" title="Ladies Pullover Batwing Sleeve Zigzag">Dark Blue Pullover Batwing Sleeve Top</a></h5>
                                    <div class="product-price-container"> <span class="product-old-price">$75.99</span> <span class="product-price">$45.99</span> </div>
                                    <!-- End .product-price-container -->
                                 </li>
                              </ul>
                              <!-- End .products-list -->
                           </div>
                           <!-- End .widget -->
                        </div>
                        <!-- End .col-md-4 -->
                     </div>
                     <!-- End .row -->
                  </div>
                  <!-- End .container -->
               </div>
               <!-- End #footer-top -->
               <div class="newsletter_subscribe parallax" style="background-image:url(images/newsletter_subscribe_bg.jpg)">
                  <div class="container">
                     <div class="row">
                        <div class="col-md-6">
                           <img src="images/mail_subscribe.png" alt="" />
                           <div class="title">
                              <h3>Subscribe for the newsletter</h3>
                              <p>Sign up for our newsletter & promotions</p>
                           </div>
                        </div>
                        <div class="col-md-6">
                           <div class="subscribe_form_wrapper">
                              <div class="input_text">
                                 <input type="email" placeholder="Your email">
                              </div>
                              <input type="submit" value="Subscribe" class="btn btn-custom2">
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="brand_wrapper">
                  <div class="container">
                     <div class="row">
                        <div class="col-lg-12">
                           <div class="owl-carousel">
                              <div class="brand"><a href="#"><img src="http://themes.halothemes.com/bicydos/site/skin/frontend/default/bicydos/images/media/brand1.png" alt=""></a></div>
                              <div class="brand"><a href="#"><img src="http://themes.halothemes.com/bicydos/site/skin/frontend/default/bicydos/images/media/brand2.png" alt=""></a></div>
                              <div class="brand"><a href="#"><img src="http://themes.halothemes.com/bicydos/site/skin/frontend/default/bicydos/images/media/brand3.png" alt=""></a></div>
                              <div class="brand"><a href="#"><img src="http://themes.halothemes.com/bicydos/site/skin/frontend/default/bicydos/images/media/brand4.png" alt=""></a></div>
                              <div class="brand"><a href="#"><img src="http://themes.halothemes.com/bicydos/site/skin/frontend/default/bicydos/images/media/brand5.png" alt=""></a></div>
                              <div class="brand"><a href="#"><img src="http://themes.halothemes.com/bicydos/site/skin/frontend/default/bicydos/images/media/brand1.png" alt=""></a></div>
                              <div class="brand"><a href="#"><img src="http://themes.halothemes.com/bicydos/site/skin/frontend/default/bicydos/images/media/brand2.png" alt=""></a></div>
                              <div class="brand"><a href="#"><img src="http://themes.halothemes.com/bicydos/site/skin/frontend/default/bicydos/images/media/brand3.png" alt=""></a></div>
                              <div class="brand"><a href="#"><img src="http://themes.halothemes.com/bicydos/site/skin/frontend/default/bicydos/images/media/brand4.png" alt=""></a></div>
                              <div class="brand"><a href="#"><img src="http://themes.halothemes.com/bicydos/site/skin/frontend/default/bicydos/images/media/brand5.png" alt=""></a></div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div id="footer-inner">
                  <div class="container">
                     <div class="row">
                        <div class="col-md-3 col-sm-6 col-xs-12">
                           <div class="widget">
                              <div class="describe-widget">
                                 <div class="footer-logo"><img src="images/Logo-02.png" alt="WooShop Logo" class="img-responsive"></div>
                                 <!-- End .footer-logo -->
                                 <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod.</p>
                                 <ul class="contact-list">
                                    <li>80 Stanley St, Redfern, Australia</li>
                                    <li><span>Phone:</span> +78 123 456 789</li>
                                    <li><span>Email:</span> support@WooShop.com</li>
                                    <li><a href="#">www.wooshop.com</a></li>
                                 </ul>
                              </div>
                              <!-- End corporate-widget -->
                           </div>
                           <!-- End .widget -->
                        </div>
                        <!-- End .col-md-3 -->
                        <div class="col-md-2 col-sm-3 col-xs-6">
                           <div class="widget">
                              <h4 class="widget-title">Support</h4>
                              <ul class="links">
                                 <li><a href="#">About us</a></li>
                                 <li><a href="#">Order History</a></li>
                                 <li><a href="#">Returns</a></li>
                                 <li><a href="#">Custom Service</a></li>
                                 <li><a href="#">Terms &amp; Condition</a></li>
                                 <li><a href="#">Order History</a></li>
                              </ul>
                           </div>
                           <!-- End .widget -->
                        </div>
                        <!-- End .col-md-2 -->
                        <div class="col-md-2 col-sm-3 col-xs-6">
                           <div class="widget">
                              <h4 class="widget-title">Important Links</h4>
                              <ul class="links">
                                 <li><a href="#">About us</a></li>
                                 <li><a href="#">Order History</a></li>
                                 <li><a href="#">Returns</a></li>
                                 <li><a href="#">Custom Service</a></li>
                                 <li><a href="#">Terms &amp; Condition</a></li>
                                 <li><a href="#">Order History</a></li>
                              </ul>
                           </div>
                           <!-- End .widget -->
                        </div>
                        <!-- End .col-md-2 -->
                        <div class="clearfix visible-sm"></div>
                        <!-- End clearfix -->
                        <div class="col-md-2 col-sm-4 col-xs-12">
                           <div class="widget">
                              <h4 class="widget-title">Our Services</h4>
                              <ul class="links">
                                 <li><a href="#">About us</a></li>
                                 <li><a href="#">Order History</a></li>
                                 <li><a href="#">Returns</a></li>
                                 <li><a href="#">Custom Service</a></li>
                                 <li><a href="#">Terms &amp; Condition</a></li>
                                 <li><a href="#">Order History</a></li>
                              </ul>
                           </div>
                           <!-- End .widget -->
                        </div>
                        <!-- End .col-md-2 -->
                        <div class="col-md-3 col-sm-8">
                           <div class="widget">
                              <h4 class="widget-title">Company working hours</h4>
                              <div class="hours-widget">
                                 <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod.</p>
                                 <ul class="hours-list">
                                    <li><span>Monday-Friday:</span> 8.30 a.m. - 5.30 p.m.</li>
                                    <li><span>Saturday:</span> 9.00 a.m. - 2.00 p.m.</li>
                                    <li><span>Sunday:</span> Closed</li>
                                 </ul>
                              </div>
                              <!-- End .company-hours-widget -->
                           </div>
                           <!-- End .widget -->
                        </div>
                        <!-- End .col-md-3 -->
                     </div>
                     <!-- End .row -->
                  </div>
                  <!-- End .container -->
               </div>
               <!-- End #footer-inner -->
               <div id="footer-bottom">
                  <div class="container">
                     <div class="row">
                        <div class="col-md-7 col-sm-6">
                           <ul class="footer-menu">
                              <li><a href="index.html">Home</a></li>
                              <li><a href="about-us.html">About</a></li>
                              <li><a href="javascript:;">Terms & Conditions</a></li>
                              <li><a href="contact.html">Contact Us</a></li>
                           </ul>
                        </div>
                        <!-- End .col-md-7 -->
                        <div class="col-md-5 col-sm-6">
                           <div class="payment-container">
                              <p class="copyright">Created with by <a href="#">Popothemes</a>. All right reserved</p>
                           </div>
                           <!-- End .payment-container -->
                        </div>
                        <!-- End .col-md-5 -->
                     </div>
                     <!-- End .row -->
                  </div>
                  <!-- End .container -->
               </div>
               <!-- End #footer-bottom -->
            </footer>
            <!-- End #footer -->
         </div>
         <!-- End #wrapper -->
         <a href="#wrapper" id="scroll-top" title="Top"><i class="fa fa-angle-up"></i></a>
         <!-- END -->
         <script src="catalog/view/theme/default/js/bootstrap.min.js"></script>
         <script src="catalog/view/theme/default/js/jquery.hoverIntent.min.js"></script>
         <script src="catalog/view/theme/default/js/jquery.slimscroll.min.js"></script>
         <script src="catalog/view/theme/default/js/waypoints.min.js"></script>
         <script src="catalog/view/theme/default/js/waypoints-sticky.min.js"></script>
         <script src="catalog/view/theme/default/js/jquery.debouncedresize.js"></script>
         <script src="catalog/view/theme/default/js/retina.min.js"></script>
         <script src="catalog/view/theme/default/js/owl.carousel.min.js"></script>
         <script src="catalog/view/theme/default/js/jquery.elevateZoom.min.js"></script>
         <script src="catalog/view/theme/default/js/jquery.bootstrap-touchspin.min.js"></script>
         <script src="catalog/view/theme/default/js/jquery.lavalamp.min.js"></script>
         <script src="catalog/view/theme/default/js/main.js"></script>
      </body>
   </html>