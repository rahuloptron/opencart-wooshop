<?php echo $header; ?>
            <!-- End #header -->
            <div id="content">
               <div class="container">
                  <ol class="breadcrumb">
                     <li><a href="index.html">Home</a></li>
                     <li class="active">Order Page</li>
                  </ol>
               </div>
               <!-- End .container -->
               <div class="container">
                  <div class="row">
                     <div class="col-md-6 col-md-offset-3">
                        <div class="form-wrapper login-box">
						
						
                          
                           <form action="index.php?route=product/productdata" method="post">
                              <div class="form-group">
                                 <label for="Name" class="input-desc">Name</label>
                                 <input type="text" class="form-control" id="Name" name="Name" required>
								 <input type="hidden" class="form-control" value="<?php echo $product_name ?>"  name="txtpname">
                              </div>
                              <!-- End .from-group -->
                              <div class="form-group">
                                 <label for="Email" class="input-desc">Email</label>
                                 <input type="Email" class="form-control" id="Email" name="Email" required>
                              </div>
                              <!-- End .from-group -->
                              
                              <!-- End .from-group -->
                              <div class="form-group">
                                 <label for="Number" class="input-desc">Mobile Number</label>
                                 <input type="Number" class="form-control" id="Number" name="Number" required>
                              </div>
                              <!-- End .from-group -->
                              <div class="xss-margin"></div>
                              <!-- space -->
                              <div class="form-group xs-margin">
                                 <input type="submit" class="btn btn-custom2" value="Register Now">
                              </div>
                              <!-- End .from-group -->
                            
                           </form>
                        </div>
                        <!-- End .form-wrapper -->
                     </div>
                     <!-- End .col-sm-6 -->
                  </div>
                  <!-- End .row -->
               </div>
               <!-- End .container -->
            </div>
            <!-- End #content -->
            <footer id="footer" class="footer-simple footer-dark">
              
               <div class="brand_wrapper">
                  <div class="container">
                     <div class="row">
                        <div class="col-lg-12">
                           <div class="owl-carousel">
                              <div class="brand"><a href="#"><img src="http://themes.halothemes.com/bicydos/site/skin/frontend/default/bicydos/images/media/brand1.png" alt=""></a></div>
                              <div class="brand"><a href="#"><img src="http://themes.halothemes.com/bicydos/site/skin/frontend/default/bicydos/images/media/brand2.png" alt=""></a></div>
                              <div class="brand"><a href="#"><img src="http://themes.halothemes.com/bicydos/site/skin/frontend/default/bicydos/images/media/brand3.png" alt=""></a></div>
                              <div class="brand"><a href="#"><img src="http://themes.halothemes.com/bicydos/site/skin/frontend/default/bicydos/images/media/brand4.png" alt=""></a></div>
                              <div class="brand"><a href="#"><img src="http://themes.halothemes.com/bicydos/site/skin/frontend/default/bicydos/images/media/brand5.png" alt=""></a></div>
                              <div class="brand"><a href="#"><img src="http://themes.halothemes.com/bicydos/site/skin/frontend/default/bicydos/images/media/brand1.png" alt=""></a></div>
                              <div class="brand"><a href="#"><img src="http://themes.halothemes.com/bicydos/site/skin/frontend/default/bicydos/images/media/brand2.png" alt=""></a></div>
                              <div class="brand"><a href="#"><img src="http://themes.halothemes.com/bicydos/site/skin/frontend/default/bicydos/images/media/brand3.png" alt=""></a></div>
                              <div class="brand"><a href="#"><img src="http://themes.halothemes.com/bicydos/site/skin/frontend/default/bicydos/images/media/brand4.png" alt=""></a></div>
                              <div class="brand"><a href="#"><img src="http://themes.halothemes.com/bicydos/site/skin/frontend/default/bicydos/images/media/brand5.png" alt=""></a></div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div id="footer-inner">
                  <div class="container">
                     <div class="row">
                        <div class="col-md-3 col-sm-6 col-xs-12">
                           <div class="widget">
                              <div class="describe-widget">
                                 <div class="footer-logo"><img src="images/Logo-02.png" alt="WooShop Logo" class="img-responsive"></div>
                                 <!-- End .footer-logo -->
                                 <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod.</p>
                                 <ul class="contact-list">
                                    <li>80 Stanley St, Redfern, Australia</li>
                                    <li><span>Phone:</span> +78 123 456 789</li>
                                    <li><span>Email:</span> support@WooShop.com</li>
                                    <li><a href="#">www.wooshop.com</a></li>
                                 </ul>
                              </div>
                              <!-- End corporate-widget -->
                           </div>
                           <!-- End .widget -->
                        </div>
                        <!-- End .col-md-3 -->
                        <div class="col-md-2 col-sm-3 col-xs-6">
                           <div class="widget">
                              <h4 class="widget-title">Support</h4>
                              <ul class="links">
                                 <li><a href="#">About us</a></li>
                                 <li><a href="#">Order History</a></li>
                                 <li><a href="#">Returns</a></li>
                                 <li><a href="#">Custom Service</a></li>
                                 <li><a href="#">Terms &amp; Condition</a></li>
                                 <li><a href="#">Order History</a></li>
                              </ul>
                           </div>
                           <!-- End .widget -->
                        </div>
                        <!-- End .col-md-2 -->
                        <div class="col-md-2 col-sm-3 col-xs-6">
                           <div class="widget">
                              <h4 class="widget-title">Important Links</h4>
                              <ul class="links">
                                 <li><a href="#">About us</a></li>
                                 <li><a href="#">Order History</a></li>
                                 <li><a href="#">Returns</a></li>
                                 <li><a href="#">Custom Service</a></li>
                                 <li><a href="#">Terms &amp; Condition</a></li>
                                 <li><a href="#">Order History</a></li>
                              </ul>
                           </div>
                           <!-- End .widget -->
                        </div>
                        <!-- End .col-md-2 -->
                        <div class="clearfix visible-sm"></div>
                        <!-- End clearfix -->
                        <div class="col-md-2 col-sm-4 col-xs-12">
                           <div class="widget">
                              <h4 class="widget-title">Our Services</h4>
                              <ul class="links">
                                 <li><a href="#">About us</a></li>
                                 <li><a href="#">Order History</a></li>
                                 <li><a href="#">Returns</a></li>
                                 <li><a href="#">Custom Service</a></li>
                                 <li><a href="#">Terms &amp; Condition</a></li>
                                 <li><a href="#">Order History</a></li>
                              </ul>
                           </div>
                           <!-- End .widget -->
                        </div>
                        <!-- End .col-md-2 -->
                        <div class="col-md-3 col-sm-8">
                           <div class="widget">
                              <h4 class="widget-title">Company working hours</h4>
                              <div class="hours-widget">
                                 <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod.</p>
                                 <ul class="hours-list">
                                    <li><span>Monday-Friday:</span> 8.30 a.m. - 5.30 p.m.</li>
                                    <li><span>Saturday:</span> 9.00 a.m. - 2.00 p.m.</li>
                                    <li><span>Sunday:</span> Closed</li>
                                 </ul>
                              </div>
                              <!-- End .company-hours-widget -->
                           </div>
                           <!-- End .widget -->
                        </div>
                        <!-- End .col-md-3 -->
                     </div>
                     <!-- End .row -->
                  </div>
                  <!-- End .container -->
               </div>
               <!-- End #footer-inner -->
               <div id="footer-bottom">
                  <div class="container">
                     <div class="row">
                        <div class="col-md-7 col-sm-6">
                           <ul class="footer-menu">
                              <li><a href="index.html">Home</a></li>
                              <li><a href="about-us.html">About</a></li>
                              <li><a href="javascript:;">Terms & Conditions</a></li>
                              <li><a href="contact.html">Contact Us</a></li>
                           </ul>
                        </div>
                        <!-- End .col-md-7 -->
                        <div class="col-md-5 col-sm-6">
                           <div class="payment-container">
                              <p class="copyright">Created with by <a href="#">Popothemes</a>. All right reserved</p>
                           </div>
                           <!-- End .payment-container -->
                        </div>
                        <!-- End .col-md-5 -->
                     </div>
                     <!-- End .row -->
                  </div>
                  <!-- End .container -->
               </div>
               <!-- End #footer-bottom -->
            </footer>
            <!-- End #footer -->
         </div>
         <!-- End #wrapper -->
         <a href="#wrapper" id="scroll-top" title="Top"><i class="fa fa-angle-up"></i></a>
         <!-- END -->
         <script src="js/bootstrap.min.js"></script>
         <script src="js/jquery.hoverIntent.min.js"></script>
         <script src="js/jquery.slimscroll.min.js"></script>
         <script src="js/waypoints.min.js"></script>
         <script src="js/waypoints-sticky.min.js"></script>
         <script src="js/jquery.debouncedresize.js"></script>
         <script src="js/owl.carousel.min.js"></script>
         <script src="js/main.js"></script>
      </body>
   </html>