<?php echo $header; ?>
            <!-- End #header -->
            <section id="content" role="main">
               <div class="container">
                  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>
               </div>
               <!-- End .container -->
               <div class="container">
                  <div class="row">
                     <div class="col-sm-6">
                        <div class="product-gallery-container">
                           <div class="product-top">
						      <?php if ($thumb) { ?>
          
          
                              <img id="product-zoom" src="<?php echo $thumb; ?>" data-zoom-image="<?php echo $thumb; ?>" href="<?php echo $popup; ?>" title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>"/>
							    <?php } ?>
                           </div>
                           <!-- End .product-top -->
                           <div class="product-gallery-wrapper">
                              <div class="owl-carousel product-gallery">
							  
							    <?php if ($images) { ?>
            <?php foreach ($images as $image) { ?>
			 <a href="<?php echo $image['popup']; ?>" title="<?php echo $heading_title; ?>" data-image="<?php echo $image['thumb']; ?>" data-zoom-image="<?php echo $image['thumb']; ?>" class="product-gallery-item">
                                 <img src="<?php echo $image['thumb']; ?>" alt="product-small-1">
                                 </a>
		
            <?php } ?>
            <?php } ?>
                                
                                
                              </div>
                              <!-- End .product-gallery -->
                           </div>
                           <!-- End #product-gallery-wrapper -->
                        </div>
                        <!-- End .product-gallery-container -->
                     </div>
                     <!-- End .col-sm-6 -->
                     <div class="col-sm-6">
                        <div class="product-details">
						
						
                           <h2 class="product-title"> <h1><?php echo $heading_title; ?></h1></h2>
						     <?php if ($manufacturer) { ?>
							 
							 
							  <?php if ($review_status) { ?>
					 <div class="product-ratings-container">
                              <div class="product-ratings">
                                 
								 <?php for ($i = 1; $i <= 5; $i++) { ?>
								  <?php if ($rating < $i) { ?>
								  <span class="star"><i class="fa fa-star-o fa-stack-1x"></i></span>
								  <?php } else { ?>
								   <span class="star active"></span>
								  <?php } ?>
								  <?php } ?>
								 
                              </div>
                              <!-- End .product-ratings -->
                              <span class="product-ratings-count"><a href="" onclick="$('a[href=\'#tab-review\']').trigger('click'); return false;"><?php echo $reviews; ?></a></span>
                           </div>		 
							 
							  <?php } ?>
			        
							  
							   <div class="product-price-container">
						    <?php if ($price) { ?>
            
                  <?php if (!$special) { ?>
				  <span class="product-price"> <?php echo $price; ?></span>
                 
                  <?php } else { ?>
				 <span class="product-old-price"><?php echo $price; ?></span><span class="product-price"><?php echo $special; ?></span>
                 
                  <?php } ?>
              
               
                <?php } ?>
						   
                             
                           </div>
							  
							  
							
                           
                           <!-- End .product-cats -->
                           
                           <!-- End .product-ratings-container -->
						   
						   <?php if ($meta_description) { ?>
                           <div style="border: 1px solid #d4d4d4;margin-bottom: 25px;padding: 10px;"><?php echo $meta_description ?></div>
						   
						   <?php } else { ?>
						   
						   <div><?php echo $meta_description ?></div>
						   
						   <?php }?>
						   
						   
						   
						   <div class="product-action">
                              <a href="index.php?route=account/registerorder&product_name=<?php echo $heading_title; ?>" class="list-btn list-btn-add" title="Add to cart">Order Now</a>
                             
                           </div>
						   
						   <div class="product-cats">
						   
						   <a href="#" title="Category Name">  <li><?php echo $text_manufacturer; ?> <a href="<?php echo $manufacturers; ?>"><?php echo $manufacturer; ?></a></li></a>
						   <?php } ?>
						   
						  
							 <li><?php echo $text_model; ?> <?php echo $model; ?></li>
            <?php if ($sku) { ?>
            
			<li><?php echo $text_sku; ?> <?php echo $sku; ?></li>
			
            <?php } ?>
            <li><?php echo $text_stock; ?> <?php echo $stock; ?></li>
						   </div>
                           
                           
                        </div>
                        <!-- End .product-details -->
                     </div>
                     <!-- End .col-sm-6 -->
                  </div>
                  <!-- End .row -->
               </div>
               <!-- End .container -->
               <div class="container">
                  <div class="row">
                     <div class="col-md-12">
                        <div role="tabpanel" class="product-details-tab">
                           <!-- Nav tabs -->
                           <ul class="nav nav-tabs" role="tablist">
                              <li role="presentation"  class="active"><a href="#description" aria-controls="description" role="tab" data-toggle="tab" title="Product Description">Product Description</a></li>
                              <li role="presentation" ><a href="#shipping" aria-controls="shipping" role="tab" data-toggle="tab" title="Shipping &amp; Returns">Specification</a></li>
                              <li role="presentation"><a href="#reviews" aria-controls="reviews" role="tab" data-toggle="tab" title="Reviews">Reviews</a></li>
                           </ul>
                           <!-- Tab Panes -->
                           <div class="tab-content">
                              <div role="tabpanel" class="tab-pane active" id="description">
                               <?php echo $description; ?>
                              </div>
                              <!-- End .tab-pane -->
                              <div role="tabpanel" class="tab-pane" id="shipping">
                                 <div class="row">
                                    <div class="col-sm-6">
                                        <table class="table table-bordered">
                <?php foreach ($attribute_groups as $attribute_group) { ?>
                <thead>
                  <tr>
                    <td colspan="2"><strong><?php echo $attribute_group['name']; ?></strong></td>
                  </tr>
                </thead>
                <tbody>
                  <?php foreach ($attribute_group['attribute'] as $attribute) { ?>
                  <tr>
                    <td><?php echo $attribute['name']; ?></td>
                    <td><?php echo $attribute['text']; ?></td>
                  </tr>
                  <?php } ?>
                </tbody>
                <?php } ?>
              </table>
                                    </div>
                                    <!-- End .col-sm-6 -->
                                    <div class="clearfix visible-xs md-margin"></div>
                                    <!-- space -->
                                
                                    <!-- End .col-sm-6 -->
                                 </div>
                                 <!-- End .row -->
                              </div>
                              <!-- End .tab-pane -->
                              <div role="tabpanel" class="tab-pane" id="reviews">
                                 <?php if ($review_status) { ?>
            <div class="tab-pane" id="tab-review">
              <form class="form-horizontal" id="form-review">
                <div id="review"></div>
                <h2><?php echo $text_write; ?></h2>
                <?php if ($review_guest) { ?>
                <div class="form-group required">
                  <div class="col-sm-12">
                    <label class="control-label" for="input-name"><?php echo $entry_name; ?></label>
                    <input type="text" name="name" value="<?php echo $customer_name; ?>" id="input-name" class="form-control" />
                  </div>
                </div>
                <div class="form-group required">
                  <div class="col-sm-12">
                    <label class="control-label" for="input-review"><?php echo $entry_review; ?></label>
                    <textarea name="text" rows="5" id="input-review" class="form-control"></textarea>
                    <div class="help-block"><?php echo $text_note; ?></div>
                  </div>
                </div>
                <div class="form-group required">
                  <div class="col-sm-12">
                    <label class="control-label"><?php echo $entry_rating; ?></label>
                    &nbsp;&nbsp;&nbsp; <?php echo $entry_bad; ?>&nbsp;
                    <input type="radio" name="rating" value="1" />
                    &nbsp;
                    <input type="radio" name="rating" value="2" />
                    &nbsp;
                    <input type="radio" name="rating" value="3" />
                    &nbsp;
                    <input type="radio" name="rating" value="4" />
                    &nbsp;
                    <input type="radio" name="rating" value="5" />
                    &nbsp;<?php echo $entry_good; ?></div>
                </div>
                <?php echo $captcha; ?>
                <div class="buttons clearfix">
                  <div class="pull-right">
                    <button type="button" id="button-review" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-primary"><?php echo $button_continue; ?></button>
                  </div>
                </div>
                <?php } else { ?>
                <?php echo $text_login; ?>
                <?php } ?>
              </form>
            </div>
            <?php } ?>
                                 <!-- End #respond -->
                              </div>
                              <!-- End .tab-pane -->
                           </div>
                           <!-- End .tab-content -->
                        </div>
                        <!-- end role[tabpanel] -->
                     </div>
                     <!-- End .col-md-12 -->
                  </div>
                  <!-- End .row -->
               </div>
               <!-- End .container -->

			  
              
			   <?php echo $content_bottom ?>
			   
			
         </div>
         <!-- End #wrapper -->
         <a href="#wrapper" id="scroll-top" title="Top"><i class="fa fa-angle-up"></i></a>
         <!-- END -->
         
		 <script type="text/javascript"><!--
$('#review').delegate('.pagination a', 'click', function(e) {
    e.preventDefault();

    $('#review').fadeOut('slow');

    $('#review').load(this.href);

    $('#review').fadeIn('slow');
});

$('#review').load('index.php?route=product/product/review&product_id=<?php echo $product_id; ?>');

$('#button-review').on('click', function() {
	$.ajax({
		url: 'index.php?route=product/product/write&product_id=<?php echo $product_id; ?>',
		type: 'post',
		dataType: 'json',
		data: $("#form-review").serialize(),
		beforeSend: function() {
			$('#button-review').button('loading');
		},
		complete: function() {
			$('#button-review').button('reset');
		},
		success: function(json) {
			$('.alert-success, .alert-danger').remove();

			if (json['error']) {
				$('#review').after('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '</div>');
			}

			if (json['success']) {
				$('#review').after('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + '</div>');

				$('input[name=\'name\']').val('');
				$('textarea[name=\'text\']').val('');
				$('input[name=\'rating\']:checked').prop('checked', false);
			}
		}
	});
});

$(document).ready(function() {
	$('.thumbnails').magnificPopup({
		type:'image',
		delegate: 'a',
		gallery: {
			enabled:true
		}
	});
});
//--></script>
    <?php echo $footer ?>