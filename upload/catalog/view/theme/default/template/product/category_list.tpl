<?php echo $header ?>

            <div id="content">
               <div class="container">
                  <ol class="breadcrumb">
                     <li><a href="index.html">Home</a></li>
                     <li class="active">Category List</li>
                  </ol>
               </div>
               <!-- End .container -->
               <div class="container">
                  <div class="row">
                     <div class="col-md-12">
                        <div class="filter-row clearfix">
                           <div class="filter-row-box">
                              <span class="filter-row-label">Sort by</span>
                              <div class="small-selectbox sort-selectbox clearfix">
                                 <select id="sort" name="sort" class="selectbox">
                                    <option value="Position">Position</option>
                                    <option value="Rate">Rate</option>
                                    <option value="Price">Price</option>
                                    <option value="Date">Date</option>
                                 </select>
                              </div>
                              <!-- End .normal-selectbox-->
                              <a href="#" class="sort-arrow" title="Sort">Sort</a>
                           </div>
                           <!-- End .filter-row-box -->
                           <div class="filter-row-box second">
                              <a href="category.html" class="btn" title="Category grid"><i class="fa fa-th"></i></a>
                              <a href="category-list.html" class="btn active" title="Category List"><i class="fa fa-th-list"></i></a>
                           </div>
                           <!-- End .filter-row-box -->
                           <div class="clearfix visible-xs"></div>
                           <!-- End .clearfix -->
                           <div class="filter-row-box last">
                              <span class="filter-row-label">Show</span>
                              <div class="small-selectbox quantity-selectbox clearfix">
                                 <select id="number" name="number" class="selectbox">
                                    <option value="12">12</option>
                                    <option value="18">18</option>
                                    <option value="24">24</option>
                                    <option value="30">30</option>
                                 </select>
                              </div>
                              <!-- End .normal-selectbox-->
                              <span class="filter-row-label hidden-xss">per page</span>
                           </div>
                           <!-- End .filter-row-box -->
                           <nav class="filter-row-box right">
                              <ul class="pagination">
                                 <li class="active"><a href="#">1</a></li>
                                 <li><a href="#">2</a></li>
                                 <li><a href="#">3</a></li>
                                 <li>
                                    <a href="#" aria-label="Next">
                                    <span aria-hidden="true"><i class="fa fa-angle-right"></i></span>
                                    </a>
                                 </li>
                              </ul>
                           </nav>
                           <!-- End .filter-row-box -->
                        </div>
                        <!-- End .filter-row -->
						
						 <?php foreach ($products as $product) { ?>
                        <div class="product product-list">
                           <div class="product-top">
                              
                                 <div class="image"><a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive" /></a></div>
                              
                           </div>
                           <!-- End .product-top -->
                           <div class="product-list-content">
                              <div class="product-cats">
                                 <a href="#" title="Category Name">Woman clothing</a>
                              </div>
                              <!-- End .product-cats -->
                              <div class="product-price-container">
                                 <span class="product-old-price"><?php echo $product['special']; ?></span>
                                 <span class="product-price"><?php echo $product['price']; ?></span>
                              </div>
                              <!-- End .product-price-container -->
                              <h3 class="product-title">
                                 <a href="<?php echo $product['href'] ?>" title="<?php echo $product['name']; ?>"><?php echo $product['name']; ?></a>
                              </h3>
                            
                              <!-- End .product-ratings -->
                              <p><?php echo $description ?></p>
                              <div class="product-action">
                                 <a href="<?php echo $product['href'] ?>" class="list-btn list-btn-add">Order Now</a>
                                 
                              </div>
                              <!-- End .product-action-wrapper -->
                              <div class="tag new">New</div>
                           </div>
                           <!-- End .product-list-content -->
                        </div>
						 <?php } ?>
                        <!-- End .product -->
                        
                        
						
                        <nav class="pagination-container">
                           <span class="pagination-info">Showing: 1-12 of 16</span>
                           <ul class="pagination">
                              <li class="active"><a href="#">1</a></li>
                              <li><a href="#">2</a></li>
                              <li><a href="#">3</a></li>
                              <li>
                                 <a href="#" aria-label="Next">
                                 <span aria-hidden="true"><i class="fa fa-angle-right"></i></span>
                                 </a>
                              </li>
                           </ul>
                        </nav>
                        <!-- End .pagination-container -->
                     </div>
                     <!-- End .col-md-12 -->
                  </div>
                  <!-- End .row -->
               </div>
               <!-- End .container -->
               <div class="xlg-margin hidden-xs hidden-sm"></div>
            </div>
            <!-- End #content -->
            <footer id="footer" class="footer-simple footer-dark">
               
               <!-- End #footer-top -->
               
               <div class="brand_wrapper">
                  <div class="container">
                     <div class="row">
                        <div class="col-lg-12">
                           <div class="owl-carousel">
                              <div class="brand"><a href="#"><img src="http://themes.halothemes.com/bicydos/site/skin/frontend/default/bicydos/images/media/brand1.png" alt=""></a></div>
                              <div class="brand"><a href="#"><img src="http://themes.halothemes.com/bicydos/site/skin/frontend/default/bicydos/images/media/brand2.png" alt=""></a></div>
                              <div class="brand"><a href="#"><img src="http://themes.halothemes.com/bicydos/site/skin/frontend/default/bicydos/images/media/brand3.png" alt=""></a></div>
                              <div class="brand"><a href="#"><img src="http://themes.halothemes.com/bicydos/site/skin/frontend/default/bicydos/images/media/brand4.png" alt=""></a></div>
                              <div class="brand"><a href="#"><img src="http://themes.halothemes.com/bicydos/site/skin/frontend/default/bicydos/images/media/brand5.png" alt=""></a></div>
                              <div class="brand"><a href="#"><img src="http://themes.halothemes.com/bicydos/site/skin/frontend/default/bicydos/images/media/brand1.png" alt=""></a></div>
                              <div class="brand"><a href="#"><img src="http://themes.halothemes.com/bicydos/site/skin/frontend/default/bicydos/images/media/brand2.png" alt=""></a></div>
                              <div class="brand"><a href="#"><img src="http://themes.halothemes.com/bicydos/site/skin/frontend/default/bicydos/images/media/brand3.png" alt=""></a></div>
                              <div class="brand"><a href="#"><img src="http://themes.halothemes.com/bicydos/site/skin/frontend/default/bicydos/images/media/brand4.png" alt=""></a></div>
                              <div class="brand"><a href="#"><img src="http://themes.halothemes.com/bicydos/site/skin/frontend/default/bicydos/images/media/brand5.png" alt=""></a></div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div id="footer-inner">
                  <div class="container">
                     <div class="row">
                        <div class="col-md-3 col-sm-6 col-xs-12">
                           <div class="widget">
                              <div class="describe-widget">
                                 <div class="footer-logo"><img src="images/Logo-02.png" alt="WooShop Logo" class="img-responsive"></div>
                                 <!-- End .footer-logo -->
                                 <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod.</p>
                                 <ul class="contact-list">
                                    <li>80 Stanley St, Redfern, Australia</li>
                                    <li><span>Phone:</span> +78 123 456 789</li>
                                    <li><span>Email:</span> support@WooShop.com</li>
                                    <li><a href="#">www.wooshop.com</a></li>
                                 </ul>
                              </div>
                              <!-- End corporate-widget -->
                           </div>
                           <!-- End .widget -->
                        </div>
                        <!-- End .col-md-3 -->
                        <div class="col-md-2 col-sm-3 col-xs-6">
                           <div class="widget">
                              <h4 class="widget-title">Support</h4>
                              <ul class="links">
                                 <li><a href="#">About us</a></li>
                                 <li><a href="#">Order History</a></li>
                                 <li><a href="#">Returns</a></li>
                                 <li><a href="#">Custom Service</a></li>
                                 <li><a href="#">Terms &amp; Condition</a></li>
                                 <li><a href="#">Order History</a></li>
                              </ul>
                           </div>
                           <!-- End .widget -->
                        </div>
                        <!-- End .col-md-2 -->
                        <div class="col-md-2 col-sm-3 col-xs-6">
                           <div class="widget">
                              <h4 class="widget-title">Important Links</h4>
                              <ul class="links">
                                 <li><a href="#">About us</a></li>
                                 <li><a href="#">Order History</a></li>
                                 <li><a href="#">Returns</a></li>
                                 <li><a href="#">Custom Service</a></li>
                                 <li><a href="#">Terms &amp; Condition</a></li>
                                 <li><a href="#">Order History</a></li>
                              </ul>
                           </div>
                           <!-- End .widget -->
                        </div>
                        <!-- End .col-md-2 -->
                        <div class="clearfix visible-sm"></div>
                        <!-- End clearfix -->
                        <div class="col-md-2 col-sm-4 col-xs-12">
                           <div class="widget">
                              <h4 class="widget-title">Our Services</h4>
                              <ul class="links">
                                 <li><a href="#">About us</a></li>
                                 <li><a href="#">Order History</a></li>
                                 <li><a href="#">Returns</a></li>
                                 <li><a href="#">Custom Service</a></li>
                                 <li><a href="#">Terms &amp; Condition</a></li>
                                 <li><a href="#">Order History</a></li>
                              </ul>
                           </div>
                           <!-- End .widget -->
                        </div>
                        <!-- End .col-md-2 -->
                        <div class="col-md-3 col-sm-8">
                           <div class="widget">
                              <h4 class="widget-title">Company working hours</h4>
                              <div class="hours-widget">
                                 <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod.</p>
                                 <ul class="hours-list">
                                    <li><span>Monday-Friday:</span> 8.30 a.m. - 5.30 p.m.</li>
                                    <li><span>Saturday:</span> 9.00 a.m. - 2.00 p.m.</li>
                                    <li><span>Sunday:</span> Closed</li>
                                 </ul>
                              </div>
                              <!-- End .company-hours-widget -->
                           </div>
                           <!-- End .widget -->
                        </div>
                        <!-- End .col-md-3 -->
                     </div>
                     <!-- End .row -->
                  </div>
                  <!-- End .container -->
               </div>
               <!-- End #footer-inner -->
               <div id="footer-bottom">
                  <div class="container">
                     <div class="row">
                        <div class="col-md-7 col-sm-6">
                           <ul class="footer-menu">
                              <li><a href="index.html">Home</a></li>
                              <li><a href="about-us.html">About</a></li>
                              <li><a href="javascript:;">Terms & Conditions</a></li>
                              <li><a href="contact.html">Contact Us</a></li>
                           </ul>
                        </div>
                        <!-- End .col-md-7 -->
                        <div class="col-md-5 col-sm-6">
                           <div class="payment-container">
                              <p class="copyright">Created with by <a href="#">Popothemes</a>. All right reserved</p>
                           </div>
                           <!-- End .payment-container -->
                        </div>
                        <!-- End .col-md-5 -->
                     </div>
                     <!-- End .row -->
                  </div>
                  <!-- End .container -->
               </div>
               <!-- End #footer-bottom -->
            </footer>
            <!-- End #footer --> 
         </div>
         <!-- End #wrapper -->
         <a href="#wrapper" id="scroll-top" title="Top"><i class="fa fa-angle-up"></i></a>
         <!-- END -->
         <script src="js/bootstrap.min.js"></script>
         <script src="js/jquery.hoverIntent.min.js"></script>
         <script src="js/jquery.slimscroll.min.js"></script>
         <script src="js/waypoints.min.js"></script>
         <script src="js/waypoints-sticky.min.js"></script>
         <script src="js/jquery.debouncedresize.js"></script>
         <script src="js/owl.carousel.min.js"></script>
         <script src="js/jquery.nouislider.all.min.js"></script>
         <script src="js/jquery.selectbox.min.js"></script>
         <script src="js/main.js"></script>
      </body>
   </html>