<?php echo $header ?>
            <div id="content">
               <div class="container">
                  <ol class="breadcrumb">
                     <li><a href="index.html">Home</a></li>
                     <li class="active">Category List</li>
                  </ol>
               </div>
               <!-- End .container -->
               <div class="container">
                  <div class="row">
                     <div class="col-md-9">
                        
                        <!-- End .filter-row -->
						 <?php foreach ($products as $product) { ?>
                        <div class="product product-list">
                           <div class="product-top">
                             
                                   <div class="image"><a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive" /></a></div>
                             
                           </div>
                           <!-- End .product-top -->

                           <div class="product-list-content">
                              
                              <!-- End .product-cats -->
                             
                              <!-- End .product-price-container -->
                              <h3 class="product-title">
                                 <a href="<?php echo $product['href'] ?>" title="<?php echo $product['name']; ?>"><?php echo $product['name']; ?></a>
                              </h3>
							  
							   <div class="product-price-container">
							   
							      
 <?php if ($product['price']) { ?>
               <p class="price">
                 <?php if (!$product['special']) { ?>
 <span class="product-price"> <?php echo $product['price']; ?></span>
                
                 <?php } else { ?>
<span class="product-old-price"><?php echo $product['price']; ?></span> <br><span class="product-price"><?php echo $product['special']; ?></span>
                
                 <?php } ?>
             
               </p>
               <?php } ?>
                               
			   
                              </div>
                              
                              <!-- End .product-ratings -->
                              <p><?php echo $product['description'] ?></p>
							  
                              <div class="product-action">
                                 <a href="<?php echo $product['href'] ?>" class="list-btn list-btn-add">Order Now</a>
                              </div>
                              <!-- End .product-action-wrapper -->
                              <span class="label label-hot rotated">NEW</span>
                           </div>
                           <!-- End .product-list-content -->
                        </div>
						 <?php } ?>
                        <!-- End .product -->
                       
                        <!-- End .product -->
                       
                        <!-- End .product -->
                        
                        <!-- End .product -->
                        <nav class="pagination-container">
                           <span class="pagination-info">Showing: 1-12 of 16</span>
                           <ul class="pagination">
                              <li class="active"><a href="#">1</a></li>
                              <li><a href="#">2</a></li>
                              <li><a href="#">3</a></li>
                              <li>
                                 <a href="#" aria-label="Next">
                                 <span aria-hidden="true"><i class="fa fa-angle-right"></i></span>
                                 </a>
                              </li>
                           </ul>
                        </nav>
                        <!-- End .pagination-container -->
                     </div>
                     <!-- End .col-md-9 -->
                     <div class="xlg-margin visible-sm visible-xs"></div>
                     <!-- space -->
                     <aside class="col-md-3 sidebar shop-sidebar">
                        <div class="widget category-widget-box">
                           <h3 class="widget-title bigger">Categories</h3>
						   
						   <?php echo $column_left ?>
                           
                        </div>
                        
                     </aside>
                  </div>
                  <!-- End .row -->
               </div>
               <!-- End .container -->
               <div class="xlg-margin hidden-xs hidden-sm"></div>
            </div>
            <!-- End #content -->
            <?php echo $footer ?>
            <!-- End #footer -->
         </div>
         <!-- End #wrapper -->
         <a href="#wrapper" id="scroll-top" title="Top"><i class="fa fa-angle-up"></i></a>
         <!-- END -->
        
      </body>
   </html>