<footer id="footer" class="footer-simple footer-dark">
              <div id="footer-bottom">
                  <div class="container">
                     <div class="row">
                        <div class="col-md-7 col-sm-6">
                           <ul class="footer-menu">
                              <li><a href="/">Home</a></li>
                              <li><a href="/about-us.html">About</a></li>
                              <li><a href="/terms.html">Terms & Conditions</a></li>
                              <li><a href="/contact.html">Contact Us</a></li>
                           </ul>
                        </div>
                        <!-- End .col-md-7 -->
                        <div class="col-md-5 col-sm-6">
                           <div class="payment-container">
                              <p class="copyright">Created by <a href="http://www.optron.in">Optron</a>. All right reserved</p>
                           </div>
                           <!-- End .payment-container -->
                        </div>
                        <!-- End .col-md-5 -->
                     </div>
                     <!-- End .row -->
                  </div>
                  <!-- End .container -->
               </div>
               <!-- End #footer-bottom -->
			   <script src="catalog/view/theme/default/js/bootstrap.min.js"></script>
         <script src="catalog/view/theme/default/js/jquery.hoverIntent.min.js"></script>
         <script src="catalog/view/theme/default/js/jquery.slimscroll.min.js"></script>
         <script src="catalog/view/theme/default/js/waypoints.min.js"></script>
         <script src="catalog/view/theme/default/js/waypoints-sticky.min.js"></script>
         <script src="catalog/view/theme/default/js/jquery.debouncedresize.js"></script>
         <script src="catalog/view/theme/default/js/retina.min.js"></script>
         <script src="catalog/view/theme/default/js/owl.carousel.min.js"></script>
         <script src="catalog/view/theme/default/js/jquery.elevateZoom.min.js"></script>
         <script src="catalog/view/theme/default/js/jquery.bootstrap-touchspin.min.js"></script>
         <script src="catalog/view/theme/default/js/jquery.lavalamp.min.js"></script>
         <script src="catalog/view/theme/default/js/main.js"></script>
		 
            </footer>