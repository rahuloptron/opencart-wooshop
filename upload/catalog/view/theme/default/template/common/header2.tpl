 <html lang="en">
      <!--<![endif]-->
	  <head>
         <meta charset="utf-8">
         <title><?php echo $title; ?></title>
		 <?php if ($description) { ?>
<meta name="description" content="<?php echo $description; ?>" />

<?php } ?>
       
         <!--[if IE]> 
         <meta http-equiv="X-UA-Compatible" content="IE=edge">
         <![endif]-->
         <meta name="viewport" content="width=device-width, initial-scale=1.0">
         <link rel="stylesheet" href="/catalog/view/theme/default/css/animate.css">
         <link rel="stylesheet" href="/catalog/view/theme/default/css/bootstrap.css">
         <link rel="stylesheet" href="/catalog/view/theme/default/css/font-awesome.min.css">
		 
         <link rel="stylesheet" href="/catalog/view/theme/default/css/themify-icons.css">
         <link rel="stylesheet" href="/catalog/view/theme/default/css/mediaelementplayer.css">
         <link rel="stylesheet" href="/catalog/view/theme/default/css/style.css">
         <!-- Favicon and Apple Icons -->
         
         <script src="/catalog/view/theme/default/js/modernizr.js"></script>
         <!--- jQuery -->
        <script src="/catalog/view/theme/default/js/jquery-2.1.3.min.js"></script>


      </head>
      <body>
         <div id="wrapper">
            <div id="mobile-menu">
               <div id="mobile-menu-wrapper">
                  <header> Navigation <a href="#" id="mobile-menu-close" title="Close Panel"></a> </header>
                  <nav>
                     <ul class="mobile-menu">
                        <li><a href="#"><i class="fa fa-angle-right"></i> Home</a></li>
                        <li>
                           <a href="#"><i class="fa fa-angle-right"></i> Shop Pages</a>
                          
                        </li>
                        <li>
                           <a href="#"><i class="fa fa-angle-right"></i> Categories</a>
                           
                              </li>
                              <li>
                                 <a href="#"><i class="fa fa-angle-right"></i>Tops category</a>
                                 
                              </li>
                              <li>
                                 <a href="#"><i class="fa fa-angle-right"></i>Dresses</a>
                                
                              </li>
                              <li>
                                 <a href="#"><i class="fa fa-angle-right"></i>Lingerie</a>
                                
                              </li>
                           </ul>
                        </li>
                        <li>
                           <a href="#"><i class="fa fa-angle-right"></i> Blog</a>
                          
                        </li>
                        <li>
                           <a href="#"><i class="fa fa-angle-right"></i> Pages</a>
                          
                        </li>
                     </ul>
                  </nav>
                  
               </div>
               <!-- End #mobile-menu-wrapper -->
            </div>
            <!-- End #mobile-menu -->
            <div id="mobile-menu-overlay"></div>
            <!-- End #mobile-menu-overlay -->
            <header id="header" class="header7">
               <div id="header-top" class="dark clearfix">
                  <div class="container">
                     <div class="nav-left">
                        <div class="header-row">
                          
                           <!-- End .language-dropdown -->
                         
                           <!-- End .currency-dropdown -->
                           </div>
                        <!-- End .header-row -->
                     </div>
                     <!-- End .nav-left -->
                     <div class="nav-right">
                        <div class="header-row">
                          
                        </div>
                        <!-- End .header-row -->
                     </div>
                     <!-- End .nav-right -->
                  </div>
                  <!-- End .container -->
               </div>
               <!-- End #header-top -->
               <div class="header_bottom">
                  <div class="container">
                     <div class="row">
                        <div class="col-lg-12 col-md-12">
                           <div class="nav-logo">
                              <h1 class="logo">
							            <?php if ($logo) { ?>
          <a href="<?php echo $home; ?>"><img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" class="img-responsive" /></a>
          <?php } else { ?>
          <h1><a href="<?php echo $home; ?>"><?php echo $name; ?></a></h1>
          <?php } ?>
                              </h1>
                              <div class="zoo_add_wrapper">
                                 <!-- Start .zoo_add_wrapper -->
                              </div>
                              <!-- End .zoo_add_wrapper -->
                           </div>
                           <!-- End .nav-logo -->
                     
                           <!-- End .header-search-container -->
                           <div class="add_to_cart_wrapper">
						   
						   <?php if ($logged) { ?>
						   
						   
						            <a class="cart_btn" href="<?php echo $logout; ?>"><?php echo $text_logout; ?></a>

						    <?php } else { ?>
							
							 <a class="cart_btn" href="<?php echo $register; ?>"><?php echo $text_register; ?></a>
							 
							  <?php } ?>
			
                           </div>
                        </div>
                        <!-- End .nav-logo -->
                     </div>
                  </div>
               </div>
               <!-- End .container -->
              
			   
			   <div id="menu-container" class="sticky-menu">
                  <div class="container">
                     <nav class="pull-left">
                        <ul class="menu rtl-dropdown">
						 <?php foreach ($categories as $category) { ?>
						         <?php if ($category['children']) { ?>
                           
                           <li>
                              <a href="javascript:;"><?php echo $category['name']; ?></a>
                              <ul>
							  
							  <?php foreach (array_chunk($category['children'], ceil(count($category['children']) / $category['column'])) as $children) { ?>
                                 <li>
								  <?php foreach ($children as $child) { ?>
                                    <a href="#"><i class="fa fa-angle-right"></i><a href="<?php echo $child['href']; ?>"><?php echo $child['name']; ?></a></a>
									<?php } ?>
                                 </li>
                                 <?php } ?>
                              </ul>
                           </li>
						   <?php } else { ?>
        <li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></li>
        <?php } ?>
						   
						   <?php } ?>
                        </ul>
                     </nav>
                  </div>
                  <!-- End .container -->
               </div>
			   
			   
            </header>
