<?php
// HTTP
define('HTTP_SERVER', 'http://127.0.0.10/');

// HTTPS
define('HTTPS_SERVER', 'http://127.0.0.10/');

// DIR
define('DIR_APPLICATION', 'C:/xampp/htdocs/opencart/upload/catalog/');
define('DIR_SYSTEM', 'C:/xampp/htdocs/opencart/upload/system/');
define('DIR_IMAGE', 'C:/xampp/htdocs/opencart/upload/image/');
define('DIR_LANGUAGE', 'C:/xampp/htdocs/opencart/upload/catalog/language/');
define('DIR_TEMPLATE', 'C:/xampp/htdocs/opencart/upload/catalog/view/theme/');
define('DIR_CONFIG', 'C:/xampp/htdocs/opencart/upload/system/config/');
define('DIR_CACHE', 'C:/xampp/htdocs/opencart/upload/system/storage/cache/');
define('DIR_DOWNLOAD', 'C:/xampp/htdocs/opencart/upload/system/storage/download/');
define('DIR_LOGS', 'C:/xampp/htdocs/opencart/upload/system/storage/logs/');
define('DIR_MODIFICATION', 'C:/xampp/htdocs/opencart/upload/system/storage/modification/');
define('DIR_UPLOAD', 'C:/xampp/htdocs/opencart/upload/system/storage/upload/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', '');
define('DB_DATABASE', 'opencart');
define('DB_PORT', '3306');
define('DB_PREFIX', 'oc_');
